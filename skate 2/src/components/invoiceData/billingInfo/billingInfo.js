import React from "react";
import "./billingInfo.css";

const BillingInfo = () => {
  return (
    <div className="billingInfo">
      <div className="row">
        <ul>
          <li className="col-md-6 col-sm-6 col-12">
            <div className="billingTo">
              <h5>Billing To</h5>
              <p>Ryets Inc.</p>
              <p>John Doe</p>
              <p>332 South Wayside Rd,</p>
              <p>Houston, TX</p>
              <p>United States</p>
            </div>
          </li>
          <li className="billingDataLi col-lg-4 col-md-5 col-sm-6 col-12">
            <div className="billing-data">
              <table>
                  <tbody>
                <tr>
                  <td>
                    <p className="ivoiceTable">Invoice number: </p>
                    <p className="dataTable"> 128734098</p>
                  </td>
                </tr>
                <tr>
                  <td>
                    <p className="ivoiceTable">Invoice date: </p>
                    <p className="dataTable"> 02 Jan 2021</p>
                  </td>
                </tr>
                <tr>
                  <td>
                    <p className="ivoiceTable">Due date: </p>
                    <p className="dataTable"> 07 Jan 2021</p>
                  </td>
                </tr>
                </tbody>
              </table>
            </div>
          </li>
        </ul>
      </div>
    </div>
  );
};
export default BillingInfo;
