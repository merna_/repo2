import React, { Fragment } from "react";
import "./diagram.css";
import Dent from "../../../../src/images/Dent.svg";
import Hail from "../../../../src/images/Hail.svg";
import overturned from "../../../../src/images/overturned-vehicle.svg";
class Diagram extends React.Component {
  constructor() {
    super();
    this.state = {
      showKittenIndex1: null,
      showKittenIndex2: null,
      showKittenIndex3: null,
      showKittenIndex4: null,
      showKittenIndex5: null,
      show: false,
      clickedOutside: false,
      frontClicked: false,
      rightClick: false,
      leftClick: false,
      topClick: false,
      backClick: false,
    };
    // {"singleDot" + " " + `${item.tops[0].name}`}

    this.toggle1 = this.toggle1.bind(this);
  }
  componentDidMount(e) {
    document.addEventListener("click", this.handleClickOutside);
  }
  componentWillUnmount() {
    document.removeEventListener("click", this.handleClickOutside);
  }
  myRef = React.createRef();
  myRef2 = React.createRef();
  myRef3 = React.createRef();
  myRef4 = React.createRef();
  myRef5 = React.createRef();

  handleClickOutside = (e) => {
    if (!this.myRef.current.contains(e.target)) {
      this.setState({
        clickedOutside: true,
        showKittenIndex1: null,
        topClick: false,
      });
    }
    if (!this.myRef2.current.contains(e.target)) {
      this.setState({
        clickedOutside: true,
        showKittenIndex2: null,
        frontClicked: false,
      });
    }
    if (!this.myRef3.current.contains(e.target)) {
      this.setState({
        clickedOutside: true,
        showKittenIndex3: null,
        backClick: false,
      });
    }
    if (!this.myRef4.current.contains(e.target)) {
      this.setState({
        clickedOutside: true,
        showKittenIndex4: null,
        leftClick: false,
      });
    }
    if (!this.myRef5.current.contains(e.target)) {
      this.setState({
        clickedOutside: true,
        showKittenIndex5: null,
        rightClick: false,
      });
    }
  };
  toggle1 = (e) => {
    this.setState({
      showKittenIndex1: e,
      clickedOutside: false,
      showKittenIndex2: null,
      showKittenIndex3: null,
      showKittenIndex4: null,
      showKittenIndex5: null,
      topClick: true,
    });
  };
  toggle2 = (e) => {
    this.setState({
      showKittenIndex2: e,
      showKittenIndex1: null,
      showKittenIndex3: null,
      showKittenIndex4: null,
      showKittenIndex5: null,
      clickedOutside: false,
      frontClicked: true,
    });
  };
  toggle3 = (e) => {
    this.setState({
      showKittenIndex3: e,
      showKittenIndex1: null,
      showKittenIndex2: null,
      showKittenIndex4: null,
      showKittenIndex5: null,
      clickedOutside: false,
      backClick: true,
    });
  };
  toggle4 = (e) => {
    this.setState({
      showKittenIndex4: e,
      showKittenIndex1: null,
      showKittenIndex2: null,
      showKittenIndex3: null,
      showKittenIndex5: null,
      clickedOutside: false,
      leftClick: true,
    });
  };
  toggle5 = (e) => {
    this.setState({
      showKittenIndex5: e,
      showKittenIndex1: null,
      showKittenIndex2: null,
      showKittenIndex3: null,
      showKittenIndex4: null,
      clickedOutside: false,
      rightClick: true,
    });
  };

  render() {
    const {
      showKittenIndex1,
      showKittenIndex2,
      showKittenIndex3,
      showKittenIndex4,
      showKittenIndex5,
    } = this.state;

    return (
      <div className="car-points">
        <div className="inspectionContainer">
          <div className="container-fluid">
            <div className="grid-container">
              {/****************************/}

              <div
                className="carContainer"
                data-face="front"
                id={this.state.frontClicked ? "clicked" : ""}
              >
                {this.props.items.map((item) => (
                  <div ref={this.myRef2}>
                    {item.fronts.map((s, i) => (
                      <Fragment>
                        <div
                          data-id={i}
                          name={s.name}
                          className="singleDot"
                          onClick={() => this.toggle2(i)}
                          style={{
                            top: `${s.top - 5}px`,
                            left: `${s.left - 5}px`,
                          }}
                        >
                          <p id="name">
                            {s.name === "damage1" ? (
                              <img
                                style={{ position: "absolute" }}
                                src={Dent}
                                alt="damage"
                              />
                            ) : s.name === "damage2" ? (
                              <img
                                style={{ position: "absolute" }}
                                src={Hail}
                                alt="damage"
                              />
                            ) : s.name === "damage3" ? (
                              <img
                                style={{ position: "absolute" }}
                                src={overturned}
                                alt="damage"
                              />
                            ) : null}
                          </p>

                          <div
                            className="smallPop"
                            id="d"
                            style={{
                              display:
                                showKittenIndex2 === i ? "block" : "none",
                            }}
                          >
                            <a target="_blank" rel="noreferrer" href="#">
                              <img
                                src="https://cdn.syarah.com/online/inspection_points/2379/0x683/1601913002-851.jpg"
                                alt="top damage "
                              />
                            </a>
                            <p>
                              <a href="c">{s.name}</a>{" "}
                            </p>
                          </div>
                        </div>
                      </Fragment>
                    ))}
                  </div>
                ))}
              </div>
              {/************************************** */}
              <div
                className="carContainer"
                data-face="right"
                id={this.state.rightClick ? "clicked" : ""}
              >
                {this.props.items.map((item) => (
                  <div ref={this.myRef5}>
                    {item.rights.map((s, i) => (
                      <Fragment>
                        <div
                          data-id={i}
                          name={s.name}
                          className="singleDot"
                          onClick={() => this.toggle5(i)}
                          style={{
                            top: `${s.top - 5}px`,
                            left: `${s.left - 5}px`,
                          }}
                        >
                          <p id="name">
                            {s.name === "damage1" ? (
                              <img
                                style={{ position: "absolute" }}
                                src={Dent}
                                alt="damage"
                              />
                            ) : s.name === "damage2" ? (
                              <img
                                style={{ position: "absolute" }}
                                src={Hail}
                                alt="damage"
                              />
                            ) : s.name === "damage3" ? (
                              <img
                                style={{ position: "absolute" }}
                                src={overturned}
                                alt="damage"
                              />
                            ) : null}
                          </p>

                          <div
                            className="smallPop"
                            id="d"
                            style={{
                              display:
                                showKittenIndex5 === i ? "block" : "none",
                            }}
                          >
                            <a target="_blank" rel="noreferrer">
                              <img
                                src="https://cdn.syarah.com/online/inspection_points/2379/0x683/1601913002-851.jpg"
                                alt="top damage "
                              />
                            </a>
                            <p>
                              <a href="c">{s.name}</a>{" "}
                            </p>
                          </div>
                        </div>
                      </Fragment>
                    ))}
                  </div>
                ))}
              </div>
              {/************************************** */}

              <div
                className="carContainer"
                data-face="top"
                id={this.state.topClick ? "clicked" : ""}
              >
                {this.props.items.map((item) => (
                  <div ref={this.myRef}>
                    {item.tops.map((s, i) => (
                      <Fragment>
                        <div
                          data-id={i}
                          name={s.name}
                          key={i}
                          className="singleDot"
                          onClick={() => this.toggle1(i)}
                          style={{
                            top: `${s.top - 5}px`,
                            left: `${s.left - 5}px`,
                          }}
                        >
                          <p id="name">
                            {s.name === "damage1" ? (
                              <img
                                style={{ position: "absolute" }}
                                src={Dent}
                                alt="damage"
                              />
                            ) : s.name === "damage2" ? (
                              <img
                                style={{ position: "absolute" }}
                                src={Hail}
                                alt="damage"
                              />
                            ) : s.name === "damage3" ? (
                              <img
                                style={{ position: "absolute" }}
                                src={overturned}
                                alt="damage"
                              />
                            ) : null}
                          </p>

                          <div
                            className="smallPop"
                            id="d"
                            style={{
                              display:
                                showKittenIndex1 === i ? "block" : "none",
                            }}
                          >
                            <a target="_blank" rel="noreferrer" href="#">
                              <img
                                src="https://cdn.syarah.com/online/inspection_points/2379/0x683/1601913002-851.jpg"
                                alt="top damage "
                              />
                            </a>
                            <p>
                              <a href="c">{s.name}</a>{" "}
                            </p>
                          </div>
                        </div>
                      </Fragment>
                    ))}
                  </div>
                ))}
              </div>

              <div
                className="carContainer"
                data-face="back"
                id={this.state.backClick ? "clicked" : ""}
              >
                {this.props.items.map((item) => (
                  <div ref={this.myRef3}>
                    {item.backs.map((s, i) => (
                      <Fragment>
                        <div
                          data-id={i}
                          name={s.name}
                          className="singleDot"
                          onClick={() => this.toggle3(i)}
                          style={{
                            top: `${s.top - 5}px`,
                            left: `${s.left - 5}px`,
                          }}
                        >
                          <p id="name">
                            {s.name === "damage1" ? (
                              <img
                                style={{ position: "absolute" }}
                                src={Dent}
                                alt="damage"
                              />
                            ) : s.name === "damage2" ? (
                              <img
                                style={{ position: "absolute" }}
                                src={Hail}
                                alt="damage"
                              />
                            ) : s.name === "damage3" ? (
                              <img
                                style={{ position: "absolute" }}
                                src="overturned-vehicle.svg"
                                alt="damage"
                              />
                            ) : null}
                          </p>

                          <div
                            className="smallPop"
                            id="d"
                            style={{
                              display:
                                showKittenIndex3 === i ? "block" : "none",
                            }}
                          >
                            <a target="_blank" rel="noreferrer">
                              <img
                                src="https://cdn.syarah.com/online/inspection_points/2379/0x683/1601913002-851.jpg"
                                alt="top damage "
                              />
                            </a>
                            <p>
                              <a href="c">{s.name}</a>{" "}
                            </p>
                          </div>
                        </div>
                      </Fragment>
                    ))}
                  </div>
                ))}
              </div>

              {/************************************** */}

              <div
                className="carContainer"
                data-face="left"
                id={this.state.leftClick ? "clicked" : ""}
              >
                {this.props.items.map((item) => (
                  <div ref={this.myRef4}>
                    {item.lefts.map((s, i) => (
                      <Fragment>
                        <div
                          data-id={i}
                          name={s.name}
                          className="singleDot"
                          onClick={() => this.toggle4(i)}
                          style={{
                            top: `${s.top - 5}px`,
                            left: `${s.left - 5}px`,
                          }}
                        >
                          <p id="name">
                            {s.name === "damage1" ? (
                              <img
                                style={{ position: "absolute" }}
                                src={Dent}
                                alt="damage"
                              />
                            ) : s.name === "damage2" ? (
                              <img
                                style={{ position: "absolute" }}
                                src={Hail}
                                alt="damage"
                              />
                            ) : s.name === "damage3" ? (
                              <img
                                style={{ position: "absolute" }}
                                src={overturned}
                                alt="damage"
                              />
                            ) : null}
                          </p>

                          <div
                            className="smallPop"
                            id="d"
                            style={{
                              display:
                                showKittenIndex4 === i ? "block" : "none",
                            }}
                          >
                            <a target="_blank" rel="noreferrer">
                              <img
                                src="https://cdn.syarah.com/online/inspection_points/2379/0x683/1601913002-851.jpg"
                                alt="top damage "
                              />
                            </a>
                            <p>
                              <a href="c">{s.name}</a>{" "}
                            </p>
                          </div>
                        </div>
                      </Fragment>
                    ))}
                  </div>
                ))}
              </div>
              {/************************************** */}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Diagram;
