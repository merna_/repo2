import React from 'react';
import { Link } from 'react-router-dom';
import './hood.css';
import $ from 'jquery';
import Scratch from "../../../../src/images/Scratch.svg"
import Dent from "../../../../src/images/Dent.svg"
import Hail from "../../../../src/images/Hail.svg"
import crack from "../../../../src/images/Crack.svg"
class Hood extends React.Component {
  state={
    type:'scratch'
  }
    highlight=()=>{
        // switch(this.state.type){
        //     case 'dent':
        //         return(
        //             console.log('d')
        //         )
        //         case 'scratch':
        //             return(
        //                 console.log('s')
        //             )
        // }
        $('.singleDot').css({"backgroundImage":'url("Dent.svg")',"backgroundColor":'red'})

    }
    render(){
    return(
        <div className="hood">
            <h5>Hood</h5>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            <div className="container-fluid">
                <div className="row">
                    <div className=" col-lg-3 col-md-4 col-sm-6 col-12">
                        <div className="item1" onClick={this.highlight}>
                            <div className="row">
                            <img src={Scratch} className="imgIcon"/>
                            <h6>{this.state.type}</h6>
                            </div>
                            <Link to="" className="hoodLink"><img className="hoodImg" src="https://cdn.syarah.com/online/inspection_points/3387/0x683/1605548221-391.jpg"/></Link>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-4 col-sm-6 col-12">
                        <div className="item1" onClick={this.highlight}>
                            <div className="row">
                            <img src={Dent} className="imgIcon"/>
                            <h6>Dent</h6>
                            </div>
                            <Link to="" className="hoodLink"><img className="hoodImg" src="https://cdn.syarah.com/online/inspection_points/3387/0x683/1605548221-391.jpg"/></Link>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-4 col-sm-6 col-12">
                        <div className="item1" onClick={this.highlight}>
                            <div className="row">
                            <img src={Hail} className="imgIcon"/>
                            <h6>Hail</h6>
                            </div>
                            <Link to="" className="hoodLink"><img className="hoodImg" src="https://cdn.syarah.com/online/inspection_points/3387/0x683/1605548221-391.jpg"/></Link>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-4 col-sm-6 col-12">
                        <div className="item1" onClick={this.highlight}>
                            <div className="row">
                            <img src={crack} className="imgIcon"/>
                            <h6>Crack</h6>
                            </div>
                            <Link to="" className="hoodLink"><img className="hoodImg" src="https://cdn.syarah.com/online/inspection_points/3387/0x683/1605548221-391.jpg"/></Link>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    )
}
}
export default Hood 