import React from 'react';
import { Link } from 'react-router-dom';
import './tail.css';

const Tail=()=>{
    return(
        <div className="tail">
            <h5>RH Tail Light</h5>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <div className="item1">
                            <div className="row">
                            <img src="dfd"/>
                            <h6>Scratch</h6>
                            </div>
                            <Link to="" className="hoodLink"><img className="hoodImg" src="https://cdn.syarah.com/online/inspection_points/3387/0x683/1605548221-391.jpg"/></Link>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <div className="item1">
                            <div className="row">
                            <img src="dfd"/>
                            <h6>Scratch</h6>
                            </div>
                            <Link to="" className="hoodLink"><img className="hoodImg" src="https://cdn.syarah.com/online/inspection_points/3387/0x683/1605548221-391.jpg"/></Link>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <div className="item1">
                            <div className="row">
                            <img src="dfd"/>
                            <h6>Scratch</h6>
                            </div>
                            <Link to="" className="hoodLink"><img className="hoodImg" src="https://cdn.syarah.com/online/inspection_points/3387/0x683/1605548221-391.jpg"/></Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Tail 