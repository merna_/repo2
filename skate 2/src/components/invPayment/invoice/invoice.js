import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import MaskGroup2 from '../../../../src/images/Mask Group 2.svg'
import './invoice.css';

const Invoice=()=>{
    useEffect(()=>{
        window.scrollTo(0,0)
    })
    return(
        <div className="invoice">
            <div className="invoice-header">
                <ul>
                    <li>
                        <div className="invoiceName">
                            <h5>Invoice# 289734567</h5>
                            <p>Pending</p>
                        </div>
                    </li>
                    <li className="right">
                        <div className="invoiceImg">
                            {/* img */}
                        </div>
                    </li>
                </ul>
            </div>
            <div className="invoiceBody">
                <div className="row">
                         <p className="to">To:</p>
                        <span>Michael Jones - Texas Auto Transport</span>
                    </div>
                    <div className="row from">
                         <p>From:</p>
                        <span>John Doe - Ryets Inc.</span>
                    </div>
                </div>
                <div className="total">
                <div className="row ">
                         <p>Total:  </p>
                        <span> $2,520.00</span>
                    </div>
                </div>
                <div className="border"></div>
                <div className="viewInvoice">
                    <Link to="/invoice" target="_blank">
                         <span>View Invoice <img src={MaskGroup2}/></span>
                         </Link>
                </div>
        </div>
    )
}
export default Invoice