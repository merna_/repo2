import { extend } from "jquery";
import React from "react";
import {Elements} from '@stripe/react-stripe-js';
import {loadStripe} from '@stripe/stripe-js';
import Payment from "./payment";
 
const stripePromise = loadStripe("pk_test_TYooMQauvdEDq54NiTphI7jx");

class PaymentHeader extends React.Component {
 
  
  render() {
    return (
      <div>
        <Elements stripe={stripePromise}>
          <Payment />
        </Elements>
      </div>
    );
  }
}
export default  PaymentHeader
