import React from "react";
import {
  ElementsConsumer,
  CardNumberElement,
  CardExpiryElement,
  CardCvcElement,
} from "@stripe/react-stripe-js";
import "./payment.css";
import $ from "jquery";
import { Link } from "react-router-dom";
import MaskGroup8 from '../../../../src/images/Mask Group 8.svg'
const CARD_ELEMENT_OPTIONS = {
  style: {
    base: {
      color: "#32325d",
      fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
      fontSmoothing: "antialiased",
      fontSize: "16px",
      "::placeholder": {
        color: "white",
      },
    },
    invalid: {
      color: "#ff0000c4",
    },
  },
};

class Payment extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      nameOnCard: "",
      cardNumber: "",
      cardExpiry: "",
      cardCvv: "",
    };
  }
  // change brand icon depend on card number
  setBrandIcon(brand) {
    var cardBrandToPfClass = {
      visa: "pf-visa",
      mastercard: "pf-mastercard",
      amex: "pf-american-express",
      discover: "pf-discover",
      diners: "pf-diners",
      jcb: "pf-jcb",
      unknown: "pf-credit-card",
    };
    var brandIconElement = document.getElementById("brand-icon");
    var pfClass = "pf-credit-card";
    if (brand in cardBrandToPfClass) {
      pfClass = cardBrandToPfClass[brand];
    }
    for (var i = brandIconElement.classList.length - 1; i >= 0; i--) {
      brandIconElement.classList.remove(brandIconElement.classList[i]);
    }
    brandIconElement.classList.add("pf");
    brandIconElement.classList.add(pfClass);
  }

  //check validation of email when changes occur

  emailChanges = (event) => {
    this.setState({
      email: event.target.value,
    });
    $(".emailLabel").removeClass("invalid");
    var emailError = document.getElementById("email-error");
    const expression = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var validEmail = expression.test(String(event.target.value).toLowerCase());
    if (!validEmail) {
      emailError.innerHTML = "email is invalid";
    } else {
      emailError.innerHTML = "";
    }
    if (event.target.value === "") {
      emailError.innerHTML = "";
    }
  };

  //check validation of name when changes occur

  nameChanges = (event) => {
    this.setState({
      nameOnCard: event.target.value,
    });
    $(".nameLabel").removeClass("invalid");
  };

    //check validation of card number when changes occur

  cardNumberChanges = (event2) => {
    var cardNumberErrors = document.getElementById("card-number-error");
    if (event2.error) {
      cardNumberErrors.innerHTML = event2.error.message;
    } else {
      cardNumberErrors.innerHTML = "";
    }
    if (event2.brand) {
      this.setBrandIcon(event2.brand);
    }
  };

    //check validation of expiry date when changes occur

  cardExpiryChanges = (event) => {
    var cardExpiryErrors = document.getElementById("card-expiry-error");
    if (event.error) {
      cardExpiryErrors.innerHTML = event.error.message;
    } else {
      cardExpiryErrors.innerHTML = "";
    }
  };

    //check validation of cvv when changes occur

  cardCvvChanges = (event) => {
    var cardCvvErrors = document.getElementById("cvv-error");
    if (event.error) {
      cardCvvErrors.innerHTML = event.error.message;
    } else {
      cardCvvErrors.innerHTML = "";
    }
  };

  
  handleSubmit = async (event) => {
    // We don't want to let default form submission happen here,
    // which would refresh the page.
    event.preventDefault();
    const { email, nameOnCard } = this.state;
    var nameError = document.getElementById("name-error");
    var emailError = document.getElementById("email-error");
    var cardExpiryErrors = document.getElementById("card-expiry-error");
    var cardNumberErrors = document.getElementById("card-number-error");
    var cardCvvErrors = document.getElementById("cvv-error");
    if (nameOnCard === "") {
      nameError.innerHTML = "name is required";
      $(".emailLabel, .nameLabel").addClass("invalid");
    } else {
      nameError.innerHTML = "";
    }
    if (email === "") {
      emailError.innerHTML = "email is required";
    } else {
      emailError.innerHTML = "";
    }

    const { stripe, elements } = this.props;
    if (!stripe || !elements) {
      // Stripe.js has not yet loaded.
      // Make  sure to disable form submission until Stripe.js has loaded.
      return;
    }

    const result = await stripe.confirmCardPayment(
      "pi_1DjnqC2eZvKYlo2CUKGb0foU_secret_6ThoGqlOg4Qq1uzvLUwvYrD7H",
      {
        payment_method: {
          card: elements.getElement(
            CardNumberElement,
            CardExpiryElement,
            CardCvcElement
          ),
          billing_details: {
            name: this.state.nameOnCard,
            email: this.state.email,
          },
        },
      }
    );

    if (result.error) {
      // Show error to your customer (e.g., insufficient funds)
      if ($("#cardNumber").hasClass("StripeElement--empty")) {
        cardNumberErrors.innerHTML = "Card Number is required";
      }
      if ($("#cardExpiry").hasClass("StripeElement--empty")) {
        cardExpiryErrors.innerHTML = "Card Expiry is required";
      }
      if ($("#cardCvv").hasClass("StripeElement--empty")) {
        cardCvvErrors.innerHTML = "Card cvv is required";
      }
      console.log(result.error.message);
    } else {
      // The payment has been processed!
      if (result.paymentIntent.status === "succeeded") {
        // Show a success message to your customer
        // There's a risk of the customer closing the window before callback
        // execution. Set up a webhook or plugin to listen for the
        // payment_intent.succeeded event that handles any business critical
        // post-payment actions.

        document.location.href = "/d";
      }
    }
  };

  render() {
    return (
      <div className="payment">
        <div className="payHeader container">
          <button className="container">
            <i className="fa fa-apple"></i> Pay
          </button>
        </div>
        <div className="or container">
          <p>Or pay with card</p>
        </div>
        <form
          onSubmit={this.handleSubmit}
          className="Form container"
          noValidate
          id="forms"
        >
          <fieldset className="FormGroup">
            <div className="input-group">
              <div className="form-group">
                <input
                  id="email"
                  type="text"
                  className="form-control"
                  required
                  onChange={this.emailChanges}
                />
                <label className="emailLabel">email</label>
              </div>
            </div>
            <div id="email-error"></div>
          </fieldset>
          <br />
          <fieldset className="FormGroup">
            <div className="input-group">
              <div className="input-group-append">
                <div className="form-group">
                  <CardNumberElement
                    id="cardNumber"
                    onChange={this.cardNumberChanges}
                    options={CARD_ELEMENT_OPTIONS}
                  />
                  <label>card number</label>
                </div>
                <div className="brand">
                  <p className="pf pf-credit-card" id="brand-icon"></p>
                </div>
              </div>
            </div>
            <div id="card-number-error"></div>
          </fieldset>
          <br />
          <fieldset className="FormGroup">
            <div className="input-group">
              <div className="form-group">
                <input
                  id="nameOnCard"
                  type="text"
                  className="form-control"
                  required
                  onChange={this.nameChanges}
                />
                <label className="nameLabel">name on card</label>
              </div>
            </div>
            <div id="name-error"></div>
          </fieldset>
          <br />
          <fieldset className="FormGroup row">
            <div className="col-md-6">
              <div className="input-group">
                <div className="form-group">
                  <CardExpiryElement
                    options={CARD_ELEMENT_OPTIONS}
                    id="cardExpiry"
                    onChange={this.cardExpiryChanges}
                  />

                  <label>Expiry Date</label>
                </div>
              </div>
              <div id="card-expiry-error"></div>
            </div>
            <div className="col-md-6 col-sm-12">
              <div className="input-group">
                <div className="input-group-append">
                  <div className="form-group">
                    <CardCvcElement
                      options={CARD_ELEMENT_OPTIONS}
                      id="cardCvv"
                      onChange={this.cardCvvChanges}
                    />
                    <label>cvv</label>
                  </div>
                  <div className="items">
                    <a href="#" className="has-tooltip">
                      <img src={MaskGroup8} id="example" />
                      <div className="container">
                        <span className="tooltip tooltip-top ">
                          is the three-digit number at the back of your debit
                          card. For certain types of debit cards, it could be a
                          four-digit number printed on the front
                        </span>
                      </div>
                    </a>
                  </div>
                </div>
              </div>{" "}
              <div id="cvv-error"></div>
            </div>
          </fieldset>
          <div className="noteBtn text-center">
            <p className="">
              By clicking "Pay", You agree to Ryets'
              <Link to=""> Terms and conditions</Link>
            </p>
            <button disabled={!this.props.stripe}>Confirm order</button>
          </div>
        </form>
      </div>
    );
  }
}

export default function InjectedPayment() {
  return (
    <ElementsConsumer>
      {({ stripe, elements }) => (
        <Payment stripe={stripe} elements={elements} />
      )}
    </ElementsConsumer>
  );
}
